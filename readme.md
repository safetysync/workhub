# Workhub Website

[![devDependency Status](https://david-dm.org/zurb/foundation-zurb-template/dev-status.svg)](https://david-dm.org/zurb/foundation-zurb-template#info=devDependencies)

This website is built using the Foundation framework and includes the following features:

- Handlebars HTML templates with Panini
- Sass compilation and prefixing
- JavaScript module bundling with webpack
- Built-in BrowserSync server

For production builds:

- CSS compression
- Unused CSS Removal
- JavaScript module bundling with webpack
- Image compression
- Auto-deployment using Firebase

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (Version 6 or greater recommended, tested with 6.11.4 and 8.12.0)
- [Git](https://git-scm.com/)

### Using the CLI

Install the Foundation CLI with this command:

```bash
npm install foundation-cli --global
```

Install the Firebase tools with:
```bash
npm install -g firebase-tools
```

Next `cd` to your project folder and run this command to setup local npm dependencies:

```bash
npm install
```

Next run this command to build the project locally:

```bash
foundation watch
```

A browser tab will appear on your localhost with the website.

Now starting coding by editing assets under the `src` folder.

### What to know about this project

Pages are 'assembled' into the dist folder which contains a pre-production version of the site. Saving any assets in the project will rebuild the project with your changes. You should see your localhost refresh a second or so after saving.

Testing via `foundation watch` is different than testing with `foundation build`. `Foundation build` has a few processes that run that optimize CSS, bundle JS, and compress images. You will find sometimes things that work on `watch` do not work with `build`. An example is that the build process deletes unused css, but exceptions for CSS that fire from JS triggers must be added in the config.yaml file otherwise they will be deleted and you'll wonder why certain animations don't work in a production build.

Foundation documentation is [found here](https://foundation.zurb.com/sites/docs/). All plugins and utilities can be used in this project, however, occasionally some are disabled(usually if we've never used it befor). Import the functionality in the scss/app.scss file by uncommenting it.

Have a look at the layouts/default.html file. This is the default layout for all pages under the pages section. Any file under the pages section is merged into the default template inside the {{> body}} tag. This is done with a tool called panini and you can see there are some ways to set exceptions there. We also use this to keep the navigation and footers separate which are located under the partials folder.

To satisfy Google SEO requirements all images must be served in next-gen formats(.webp) WITH lazy loading images on page scroll. We use the `picture` tag combined with an [Intersection Observer API](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API).

Images should be setup like so:
```html
<picture>
    <source srcset="https://via.placeholder.com/1" srcset1="/img/nextGenImage.webp" type="image/webp">
    <img src="/assets/img/fallbackImage.png" alt="Workhub Logo">
</picture>
```

We use the Intersection Observer to load the placeholder image when an image is outside the users viewport and then insert the webp image only when the user scrolls to the area containing the image. The .png image is needed for browsers that don't support .webp or the Intersection Observer such as IE.

Lastly, have a look in the .vscode folder for snippets that I've created to help speed up coding.

### How to publish

1. Run the Build Process
2. Deploy to Firebase

```bash
foundation build
```

This will run the production build process and place the final version in the dist folder(auto replacing any pre-production assets there)

Next you need to run:
```bash
firebase deploy
```

Running this command will deploy the website to our staging site AND the production site. If you want to specify a site to publish to use:

```bash
firebase deploy --only hosting:prod
```

or

```bash
firebase deploy --only hosting:staging
```

Obviously the production website is located at https://www.workhub.com but the staging website is located at https://staging-workhub.web.app/

You can shorten this process by using:

```bash
foundation build && firebase deploy --only hosting:prod
```

If you've never published before you may need to use `firebase login` to authenticate your Firebase credentials. 
