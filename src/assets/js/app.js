import $ from 'jquery';
import 'what-input';
import EmblaCarousel from 'embla-carousel';
import './lib/sticky-footer.js'

// ----- IMAGE CAROUSEL ---------------------------------------------

const emblaNode = document.querySelector('.embla');
let embla;

if (emblaNode) {
  embla = EmblaCarousel(emblaNode, {
    align: 'center',
    containerSelector: '*',
    slidesToScroll: 1,
    containScroll: false,
    draggable: true,
    dragFree: false,
    loop: true,
    speed: 10,
    startIndex: 0,
    selectedClass: 'is-selected',
    draggableClass: 'is-draggable',
    draggingClass: 'is-dragging',
  });

  var activeIndex = 0;
  var activeFeatures = document.getElementsByClassName('animate-path');
  var interval;
  var isAutoAnimating = true;

  const carouselBtns = document.getElementsByClassName('features-btn');
  for (let i = 0; i < carouselBtns.length; i++) {
    carouselBtns[i].addEventListener('click', function () {
      embla.scrollTo(carouselBtns[i].id);
      setActiveFeature();
      endAutoScroll();
    }, false);
  }

  embla.on('dragStart', function () {
    setActiveFeature();
    if (isAutoAnimating) {
      endAutoScroll();
      isAutoAnimating = false;
    }
  });

  embla.on('dragEnd', function () {
    setActiveFeature();
  });
}

function endAutoScroll() {
  clearInterval(interval);
  for (let i = 0; i < activeFeatures.length; i++) {
    activeFeatures[i].classList.add('no-animate');
  }
}

function setActiveFeature() {
  activeFeatures[activeIndex].classList.remove('is-animate-path-active');
  activeIndex = +embla.selectedScrollSnap();
  activeFeatures[activeIndex].classList.add('is-animate-path-active');
}

// Foundation JS relies on a global varaible. In ES6, all imports are hoisted
// to the top of the file so if we used import to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;

// require('foundation-sites');

// If you want to pick and choose which modules to include, comment out require foundation sites above and uncomment
// the line below
import './lib/foundation-explicit-pieces';

$(document).foundation();

// ----- INTERSECTION OBSERVER ---------------------------------------------

const options = {
  rootMargin: "25px",
  threshold: 0.1
};
const io = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    // console.log('entry: ', entry);
    if (entry.intersectionRatio > 0.1) {

      // Start carousel interval
      if (entry.target.classList.contains('features-btn-array')) {
        activeFeatures[activeIndex].classList.add('is-animate-path-active');
        interval = window.setInterval(function () {
          embla.scrollNext();
          setActiveFeature();
        }, 5000);
        io.unobserve(entry.target);
      }

      // Lazy load images
      if (entry.target.tagName === 'SOURCE') {

        // Load all carousel images at once
        if (entry.target.classList.contains('carousel-img')) {
          var carouselImages = document.querySelectorAll('.carousel-img');
          for (let image of carouselImages) {
            lazyLoadImage(image);
          }
        } else {
          lazyLoadImage(entry.target);
        }
      }
    }
  });
}, options);

const targetElements = document.querySelectorAll("source");
for (let element of targetElements) {
  // console.log('element: ', element);
  io.observe(element);
}
const element = document.querySelector('.features-btn-array');
if (element) {
  io.observe(element);
}

function lazyLoadImage(element) {
  var imgsrc = element.getAttribute("srcset1");
  element.setAttribute("srcset", imgsrc);
  element.removeAttribute("srcset1");
  io.unobserve(element);
}